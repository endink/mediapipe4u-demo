// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MediaPipe4UDemoGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MEDIAPIPE4UDEMO_API AMediaPipe4UDemoGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
